package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"
)

func main() {

	var concurrency int
	var payloads string
	var match string
	var appendMode bool
	var paths bool
	var silent bool
	flag.IntVar(&concurrency, "c", 30, "Set concurrency for better speed")
	flag.StringVar(&payloads, "pL", "", "This is the payloads list")
	flag.StringVar(&match, "m", "", "Match the response with a pattern (e, g) Success:")
	flag.BoolVar(&appendMode, "a", false, "Append the payload to the parameter")
	flag.BoolVar(&paths, "p", false, "Only test ssrf in paths")
	flag.BoolVar(&silent, "s", false, "Only print vulnerable hosts")
	flag.Parse()

	if payloads != "" {

		var wg sync.WaitGroup
		for i := 0; i <= concurrency; i++ {
			wg.Add(1)
			go func() {
				test_ssrf(payloads, match, appendMode, silent, paths)
				wg.Done()
			}()
			wg.Wait()
		}
	}
}

// usado para testeos de ssrf
func test_ssrf(payloads string, match string, appendMode bool, silent bool, paths bool) {

	file, err := os.Open(payloads)

	if err != nil {
		log.Fatal("File could not be read")
	}

	defer file.Close()

	time.Sleep(time.Millisecond * 10)
	scanner := bufio.NewScanner(os.Stdin)

	pScanner := bufio.NewScanner(file)

	for scanner.Scan() {
		for pScanner.Scan() {
			link := scanner.Text()
			payload := pScanner.Text()

			u, err := url.Parse(link)
			if err != nil {
				return
			}

			fmt.Println(paths)
			if paths == false {

				qs := url.Values{}
				for param, vv := range u.Query() {
					if appendMode {
						qs.Set(param, vv[0]+payload)
					} else {
						qs.Set(param, payload)
					}
				}

				u.RawQuery = qs.Encode()
				if silent == false {
					fmt.Printf("[+] Testing \t %s\n", u)
				}
				make_request(u.String(), match)

			} else {
				newLink := link + payload
				if silent == false {
					fmt.Printf("[+] Testing \t %s\n", newLink)
				}
				make_request(newLink, match)
			}
		}
	}
}

// MAKING A REQUEST FOR CHECKING OUTPUTS FOR VULNERABILITIES
func make_request(url string, match string) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}

	resp, err := client.Do(req)
	if err != nil {
		return
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	bodyString := string(bodyBytes)

	if strings.Contains(bodyString, match) {
		fmt.Println(url + " IS VULNERABLE")
	}

}
